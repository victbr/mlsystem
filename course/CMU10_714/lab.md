# lab notes

## hw0


### python文件处理

`parse_mnist(image_filename, label_filename)`: 

* `gzip`打开gz格式文件。 

* 二进制文件大小端问题，用`struct`库解决。 `struct.unpack('>IIII', f.read(16))`，第一个参数为format，第二个为输入的二进制数据，通过`file.read()`获取。`>`表示大端法，`I`表示4byte无符号整数。

* `numpy.frombuffer(f.read(), dtype='B').astype(numpy.uint8)`:从f.read()获得剩余的数据流， 按Byte类型获取数据，然后转成numpy结构，类型从Byte转成uint8。（一个byte所以大小端无关？）

### numpy

* matmul和dot的区别

matmul(符号`@`)将多维矩阵看成将一批2D矩阵叠放。matmul就是将两批2D矩阵逐个做。`(2,3,4,5) @ (2,3,5,1)`得到`(2,3,4,1)`的结果。如果批数不相等，则将小的那个做broadcast，做不了broadcast看成失败。

dot也是看成两批矩阵，但是两批矩阵中两两会做乘积，而不会做配对。`(2,3,4,5) @ (2,3,5,1)`得到`(2,3,4,2,3,1)`

* maximum和max

maximum是做pairwise求max， max类似于sum，按照某几维求max。

### bind c++ 

pybind11/pybind11.hw0
pybind11/numpy.h 

先make成动态链接库后再在python里面import， 需要参数`$$(python -m pybind11 --includes)`.

## hw1

实现needle

### ops

* class的对象如果有`__call__`函数，那么可以当成函数来调用。相当于重载了`(...)`运算符。

* 函数如果返回多个值。`a = f(x)`后a会是一个tuple。如果只返回一个值那么会看成返回的类型。 `return x`的话a会是x。`return x, `则`a=(x,)`，是一个tuple。 此时`a, = f(x)`的话a会是x。

* transpose

numpy的transpose默认会把矩阵的所有维度取反，即原本shape是(1,3,2,4)的矩阵会变成(4,2,3,1)。 axes参数是一个置换，即按照axes里的顺序放置每一个维度。默认`axes=None`表示逆序即`arange(n)[::-1]`。 若`axes=(1,2,3,0)`则(1,3,2,4)会变成(3，2，4，1)。

而hw1里的transpose默认交换最后两维，或者axes给一个`tuple(axis1,axis2)`交换给定的两维。对应matmul需要的转置。

* broadcastTo

将一个矩阵broadcast到一个新的shape。(3,1) 可以broadcast到(2，3，5)。

规则可以理解为，首先在现在的矩阵维度前面补1，直到和target shape维数一样。然后每一位要么是1，要么和target一样。然后沿着值为1的维度broadcast到target值。

* summation

沿着给定的几个维度求和。给定的维度axes可能是单个值，None，或者tuple。反向传播时需要把out_grad的值broadcast到对应的位置上。summation会导致维度减少，需要在减少的位置补1然后再broadcast。例如(3,4,5)沿着dim=1求和变成(3,5)，需要先补到(3,1,5)。可以用reshape实现这一步。

* matmul

注意两边高维不一样的情况。需要沿着对应维度求和。

例如`a=(3,1,3,5), b = (1,3,5,2)`，`a@b`得到`(3,3,3,2)`

* ReLU

需要用到node forward的结果。可以通过`realize_cached_data()`得到。这个函数会缓存结果，避免重复计算。

### 其他

topo order可以通过搜索得到。不一定需要计算度数每次取0度点。
