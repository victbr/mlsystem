
## distributed training system

Eurosys22 Varuna: Scalable, Low-cost Training of Massive Deep Learning Models


SC21 Efficient Large-Scale Language Model Training on GPU Clusters Using Megatron-LM

MLSys22 Pathways: Asynchronous Distributed Dataflow for ML

SC22 LightSeq2: Accelerated Training for Transformer-Based Models on GPUs

OSDI22 Alpa: Automating Inter- and Intra-Operator Parallelism for Distributed Deep Learning

PMLR21 PipeTransformer: Automated Elastic Pipelining for Distributed Training of Large-scale Models

ASPLOS23 Mobius: Fine Tuning Large-Scale Models on Commodity GPU Servers

ASPLOS23 Optimus-CC: Efficient Large NLP Model Training with 3D Parallelism Aware Communication Compression

SC20 ZeRO: Memory optimizations Toward Training Trillion Parameter Models

Using DeepSpeed and Megatron to Train Megatron-Turing NLG 530B, A Large-Scale Generative Language Model

FastMoE: A Fast Mixture-of-Expert Training System

Megatron-LM: Training Multi-Billion Parameter Language Models Using Model Parallelism