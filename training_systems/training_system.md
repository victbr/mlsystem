# system

## inter-layer/pipeline partitioning
PipeDream
GPipe

## intra-layer partitioning
Mesh-Tensorflow
Megatron
Turing-NLG

## Memory optimization
ZeRO/DeepSpeed 

## not detected
PipeMare
DAPPLE
Spotnik
MindSpore

## Resource elasticity and Spot VMs
PyTorch Elastic
MXNet Dyanmic
Proteus
Spotnik


